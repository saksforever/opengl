from math import acos, atan, degrees, sqrt
import math
from OpenGL.GL import *  
from OpenGL.GLU import *  
from OpenGL.GLUT import *  
import numpy as np


class Octahedron:
    x = -0.4
    y = 1.2
    z = -0.3

    edges = np.array(
        [
            [0, 1],
            [0, 2],
            [0, 3],
            [0, 4],
            [5, 1],
            [5, 2], #5
            [5, 3],
            [5, 4],
            [1, 2],
            [2, 3], #9
            [3, 4],
            [4, 1]
        ]
    )

    selected_edge = 1  # номер выбранного ребра
    return_second = True

    scale = [0.4, 0.4, 0.4]

    vertices = np.array(
        ((0, 0, -1), (1, 0, 0), (0, 1, 0), (-1, 0, 0), (0, -1, 0), (0, 0, 1))
    )

    faces = (
        (0, 1, 2), 
        (0, 2, 3),  
        (0, 3, 4),  
        (0, 4, 1),  
        (5, 1, 2),  
        (5, 2, 3),  
        (5, 3, 4),
        (5, 4, 1),
    )

    texcoords = ((0, 0), (1, 0), (1, 1), (0, 1))

    # Идентификатор текстуры
    texture_id = 1

    def __init__(self):
        ...

    def set_texture_id(self, id):
        self.texture_id = id

    def display(self):
        glTranslatef(
            self.x,
            self.y,
            self.z,
        )
        glScale(*self.scale)
        glRotatef(60, 1, 0, 1) # поворот чтобы изначально лежал на грани 
        self.__draw_cube()

    # Euler-Rodrigues formula
    def rotate(self, angle, vert1, vert2):
        axis = self.vertices[vert1] - self.vertices[vert2]
        axis = axis / np.linalg.norm(axis)
        theta = np.radians(angle)

        axis = np.asarray(axis)
        axis = axis / math.sqrt(np.dot(axis, axis))
        a = math.cos(theta / 2.0)
        b, c, d = -axis * math.sin(theta / 2.0)
        aa, bb, cc, dd = a * a, b * b, c * c, d * d
        bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
        rot_mat = np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                            [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                            [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])

        # применяем матрицу вращения на все точки
        self.vertices = np.dot(self.vertices, rot_mat)

    def __draw_cube(self):
        # включения поддержки текстурированного объекта
        glEnable(GL_TEXTURE_2D)

        glBindTexture(GL_TEXTURE_2D, self.texture_id)
        glBegin(GL_TRIANGLES)
        for face in self.faces:
            for i, vertex in enumerate(face):
                glTexCoord2fv(self.texcoords[i])
                glVertex3fv(self.vertices[vertex])
        glEnd()
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
        # отключение поддержки текстурированного объекта
        glDisable(GL_TEXTURE_2D)


octahedron = Octahedron()
