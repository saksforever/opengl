from OpenGL.GL import *


class Surface:
    def __init__(self, *, color, scale):
        self.color = color
        self.scale = scale
    
  
    def set_texture_id(self, id):
        self.texture_id = id
  
  
    def display(self):
        glEnable(GL_TEXTURE_2D)
        glBindTexture(GL_TEXTURE_2D, self.texture_id)
    
        glScale(self.scale, self.scale, self.scale)
        glBegin(GL_QUADS)
        
        glTexCoord(1, 1)
        glVertex3f(1, -0.2, 1)
        glTexCoord(1, 0)
        glVertex3f(1, -0.2, -1)
        glTexCoord(0, 0)
        glVertex3f(-1, -0.2, -1)
        glTexCoord(0, 1)
        glVertex3f(-1, -0.2, 1)
        glEnd()
        
        glDisable(GL_TEXTURE_2D)

surface = Surface(color=(1, 0.6, 0.5), scale=5)