from OpenGL.GL import *  
from OpenGL.GLU import *  
from OpenGL.GLUT import *
import numpy as np  

from core.keys_handler import keyboard_handler
from config.animate_settings import timer_settings

from figures.octahedron import octahedron


# анимации
def timer_func(val):
    """
    void glutTimerFunc(unsigned int msecs,
                   void (*func)(int value), value);

    glutTimerFunc registers the timer callback func to be triggered in at least msecs milliseconds. The value parameter to the timer callback will be the value of the value parameter to glutTimerFunc. Multiple timer callbacks at same or differing times may be registered simultaneously.

    The number of milliseconds is a lower bound on the time before the callback is generated. GLUT attempts to deliver the timer callback as soon as possible after the expiration of the callback's time interval.

    There is no support for canceling a registered callback. Instead, ignore a callback based on its value parameter when it is triggered.
    """
    angleE = 0

    if not timer_settings.animate:
        glutTimerFunc(500, timer_func, 0)
        return

    # если превышено максимально время - сброс анимации, а также ее остановка (повторная возможна на пробел)
    if timer_settings.timer_cur > timer_settings.timer_max:
        # stop animate, return to init position
        keyboard_handler(b" ", None, None)
        timer_settings.timer_cur = 0

        octahedron.x = -0.4
        octahedron.y = 1.2
        octahedron.z = -0.3
        angleE = 0
        octahedron.vertices = np.array(
        ((0, 0, -1), (1, 0, 0), (0, 1, 0), (-1, 0, 0), (0, -1, 0), (0, 0, 1))
    )

        glutPostRedisplay()
    else:
        timer_settings.timer_cur += 1

    print(timer_settings.timer_cur)

    if timer_settings.animate:
        if timer_settings.timer_cur < 70:
            angleE += 1
            octahedron.rotate(angleE, 1, 2)
            octahedron.x -= 0.001
            octahedron.z -= 0.001
        elif timer_settings.timer_cur > 70 and timer_settings.timer_cur < 72:
            angleE = 0
        elif timer_settings.timer_cur > 72 and timer_settings.timer_cur < 142:
            angleE -= 1
            octahedron.rotate(angleE, 2, 5)
            octahedron.x += 0.00142
            octahedron.z -= 0.00285
        elif timer_settings.timer_cur > 142 and timer_settings.timer_cur < 144:
            angleE = 0
        elif timer_settings.timer_cur > 144 and timer_settings.timer_cur < 214:
            angleE -= 1
            octahedron.rotate(angleE, 0, 1)
            octahedron.x -= 0.00142
            octahedron.z -= 0.00285
        elif timer_settings.timer_cur > 214 and timer_settings.timer_cur < 216:
            angleE = 0
        elif timer_settings.timer_cur > 217 and timer_settings.timer_cur < 287:
            angleE += 1
            octahedron.rotate(angleE, 1, 2)
            octahedron.x += 0.00142
            octahedron.z -= 0.00285
        elif timer_settings.timer_cur > 288 and timer_settings.timer_cur < 290:
            angleE = 0
        elif timer_settings.timer_cur > 291 and timer_settings.timer_cur < 362: #431:
            angleE -= 1
            octahedron.rotate(angleE, 2, 5)
            octahedron.x += 0.001
            octahedron.z -= 0.002
        elif timer_settings.timer_cur > 362 and timer_settings.timer_cur < 450:
            angleE += 1
            octahedron.rotate(angleE, 1, 2)
            octahedron.z -= 0.015
            octahedron.y -= 0.022
        elif timer_settings.timer_cur > 451 and timer_settings.timer_cur < 500:
            angleE += 1
            octahedron.rotate(angleE, 2, 5)
    glutPostRedisplay()
    glutTimerFunc(10, timer_func, 0)
