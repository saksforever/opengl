from OpenGL.GL import *  
from OpenGL.GLU import *  
from OpenGL.GLUT import *  

from config.settings import settings  
from core import display  
from core.animation import timer_func
from core.keys_handler import special_keys_handler, keyboard_handler  


def main():
   # настройка opengl
   settings()

   # отрисовка
   glutDisplayFunc(display.display)

   # триггер при нажатии на клавиши
   glutSpecialFunc(special_keys_handler)
   glutKeyboardFunc(keyboard_handler)

   # анимация
   glutTimerFunc(500, timer_func, 0)

   # Запускаем основной цикл
   glutMainLoop()

if __name__ == "__main__":
   main()
